<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Library\TextHelper;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hashtags', function () {
    $text = "this has a The text you want to filter goes here. http://google.com/#hashtag a  #badhash-tag and a #goodhash_tag";
    $text2 = "The text you want to filter goes here. http://google.com";
    return "<pre>**Text: $text\nHashtags: \n".var_export(TextHelper::getHashTags($text),true)."\n\n".
        "**Text 2: $text2\nLinks: \n".var_export(TextHelper::getLinks($text2),true);
});

Route::get('/user', [
    'uses' => 'UserController@index'
]);
