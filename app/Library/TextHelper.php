<?php

namespace App\Library;

class TextHelper{

    /**
     * Get all hash tags from text with start and end indexes
     *
     * @param $text
     * @return array
     */
    public static function getHashTags($text)
    {
        preg_match_all("/((\s|^)#\w+)/", $text, $matches);

        $matches[0] = array_map(function ($v){
            return trim($v);
        },$matches[0]);

        return self::getIndexes($text, $matches[0]);
    }

    /**
     * Get all links from text with start and end indexes
     *
     * @param $text
     * @return array
     */
    public static function getLinks($text)
    {
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $text, $matches);

        return self::getIndexes($text, $matches[0]);
    }

    /**
     * Find start and end index of phrases
     *
     * @param $text
     * @param array $phrases
     * @return array
     */
    private static function getIndexes($text, array $phrases)
    {
        if(!$phrases)
            return [];

        $result = [];
        $end_pos = 0;
        $i=0;
        while (($pos = strpos($text, $phrases[$i],$end_pos)) !== false) {
            $end_pos = $pos + strlen($phrases[$i]);

            $result[] = [
                'phrase' => substr($text,$pos,$end_pos-$pos),
                'start' => $pos,
                'end'=> $end_pos
            ];

            $i++;
            if(!isset($phrases[$i]))
                break;
        }

        return $result;
    }
}