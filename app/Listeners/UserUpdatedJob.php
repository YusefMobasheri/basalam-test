<?php

namespace App\Listeners;

use App\Events\UserUpdated as UserUpdatedEvent;
use App\Jobs\UserChangeJob;

class UserUpdatedJob
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserUpdatedEvent $event)
    {
        UserChangeJob::dispatch($event->user)
            ->delay(now()->addMinutes(5));
    }
}
