<?php

namespace App\Listeners;

use App\Events\UserUpdated as UserUpdatedEvent;


class UserUpdated
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserUpdatedEvent $event)
    {
        app('log')->info($event->user);
    }
}
