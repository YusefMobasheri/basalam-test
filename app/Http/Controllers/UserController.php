<?php

namespace App\Http\Controllers;

use App\Jobs\UserChangeJob;
use App\Library\TextHelper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class UserController extends Controller
{
    public function index()
    {
        /** @var User $user */
        $user = User::find(1);
        $user->name = 'yusef';
        $user->update();
    }
}
